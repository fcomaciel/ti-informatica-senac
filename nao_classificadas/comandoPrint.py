# De fato o PRINT é uma função e não um comando.

print('Senac', 'Piauí', sep=' - ', end='')
print('Curso', 'Técnico de Informática', sep=' - ')

print('Praticando com a função print', end='')
print('da linguagem de programação Python')

# Só lembrando, o Python diferencia letras maiúsculas e minúsculas

# Print('Teste 123')

"""
EXERCÍCIO:
Utilizando a função PRINT com  SEP= imprima na tela o seguinte CPF
824.176.070-18
"""

print('CPF: 824', '176', '070', sep='.', end='-')
print('18')
