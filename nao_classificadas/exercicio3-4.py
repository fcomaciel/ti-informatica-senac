"""
Considere que as variáveis "dia", "mês" e "ano" contém os valores
respectivos de uma certa data. Escreva um comando "print" usando a
mensagem "format" que imprima essa data no formato usual, por
exemplo, "12/11/2016" ou "3/7/2011".
"""
dia = 12
mes = 11
ano = 2016

print('{0}/{1}/{2}'.format(dia, mes, ano))
