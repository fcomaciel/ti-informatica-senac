"""
Transforme os comandos a seguir para a notação que usa a mensagem
"format". Lembre-se que argumentos separados por vírgula adicionam
um espaço entre eles na impressão:

- print(x, 'e', y, 'são',a, 'e', b)
- print('Total:', x + y)
- print(a, b e c, 'são números inteiros)
- print(z,'+ O =',z)
"""

a = 18
b = 7
c = 3
x = a
y = b
z = a + b + c

print('x e y são {0} e {1}'.format(x, y))
print('Total: {0}'.format(x + y))
print('{0}, {1} e {2} são números inteiros'.format(a, b, c))
print('{0} + 0 = {1}'.format(z, z + 0))
