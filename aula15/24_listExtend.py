""" Listas em Python: extend, + """
lista1 = [1, 2, 3]
lista2 = [4, 5, 6]
print('Lista1:', lista1)
print('Lista2:', lista2)

lista3 = lista1 + lista2  # Concatenando lista1 e lista2
print('Lista3 concatenada (lista1 + lista2):', lista3)
lista1.extend(lista2)  # Acrescenta a lista2 na lista1
print('Lista1 estendida:', lista1)

lista4 = [7, 8, 9]
lista4.extend('A')  # Acrescenta 'A' a lista1
print('Lista4 estendida de "A":', lista4)
