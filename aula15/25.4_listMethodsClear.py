""" Listas em Python: Método: clear """
lista1 = [1, 2, 3, 4, 5, 6]
lista2 = [7, 8, 9, 10, 11, 12]
print(f'Lista1: {lista1}')
print(f'Lista2: {lista2}')
lista2.clear()  # clear, limpa a lista
print('Lista2 limpa:', lista2)
