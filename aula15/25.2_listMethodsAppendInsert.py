"""
Listas em Python
Métodos: append, insert
"""
lista1 = [1, 2, 3, 4, 5, 6]
lista2 = [7, 8, 9, 10, 11, 12]
print(f'Lista1: {lista1}')
print(f'Lista2: {lista2}')
lista1.append('a')  # append, acrescente um elemento ao final da lista
print('append:', lista1)
lista2.insert(2, 'Chocolate')  # insert, insere um elemento posicionado
print('insert:', lista2)
