""" Listas em Python: Métodos: pop, del """
lista1 = ['a', 'b', 'c', 'd', 'e', 'f']
lista2 = [7, 8, 9, 10, 11, 12]
print(f'Lista1: {lista1}')
print(f'Lista2: {lista2}')
lista1.pop()  # pop, exclui o último elemento da lista
print('pop:', lista1)
del(lista2[-1])  # del, exclui um elemento posicionado
print('del:', lista2)
