"""
Listas em Python
Métodos: max, min
"""
lista1 = [1, 6, 2, 3, 4, 5]
lista2 = [8, 9, 10, 11, 7, 12]
print(f'Lista1: {lista1}')
print(f'Lista2: {lista2}')
print(f'Valor máximo lista1: {max(lista1)}')
print(f'Valor mínimo lista2: {min(lista2)}')
