"""
Listas em Python
- max, min, clear, append, insert, pop, del

Documentação completa em:
https://docs.python.org/pt-br/3/tutorial/datastructures.html
"""
lista1 = [1, 2, 3, 4, 5, 6]
lista2 = [7, 8, 9, 10, 11, 12]

print(f'Lista1: {lista1}')
print(f'Lista2: {lista2}')

print(f'Valor máximo lista1: {max(lista1)}')
print(f'Valor mínimo lista2: {min(lista2)}')

# lista2.clear()  # clear, limpa a lista
# print(lista2)

lista1.append('a')  # append, acrescente um elemento ao final da lista
print('append:', lista1)

lista2.insert(0, 'Chocolate')  # insert, insere um elemento posicionado
print('insert:', lista2)

lista1.pop()  # pop, exclui o último elemento da lista
print('pop:', lista1)

del(lista2[-1])  # del, exclui um elemento posicionado
print('del:', lista2)
