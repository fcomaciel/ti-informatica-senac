"""
Split, Join e Enumerate em Python
1. Split - usado para dividir uma string (str)
2. Join - Usado para juntar uma lista (str)
3. Enumerate - Usado para enumerar elementos de objetos iteráveis
"""

string_a = 'Python é fenomenal, Python é para propósito geral'

lista_a = string_a.split(' ')

# Lista com todos os elementos da string_a
print(f'Lista_a: {lista_a}')

# Contendo 2 elementos resultante de string_a.split(', ')
lista_b = string_a.split(', ')

print(f'Lista_b: {lista_b}')
