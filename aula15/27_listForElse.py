"""
FOR / ELSE, startswith e end='') em Python
"""

var_lista = [
    'Ruby', 'Java', 'C++', 'Python', 'C#', 'JavaScript', 'C'
    ]

# for valor in var_lista:
#     print(valor)
#     continue
#     # break
#     print(valor)
# else:
#     print('Fim do laço for!')

print(len(var_lista))

for valor in var_lista:
    # print(valor)
    print(valor, end=' ')
    # print(valor, end=': ')
    if valor.lower().startswith('c'):
        print('Inicia com C')
    else:
        print('Não inicia com C')
