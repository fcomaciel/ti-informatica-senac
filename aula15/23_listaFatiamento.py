"""
Listas em Python
- fatiamento
"""
# Listas também têm índices.
# E se tem índice, é iterável.
#         0    1    2    3    4   5  6  7
lista = ['A', 'B', 'C', 'D', 'E', 1, 2, 3]
#        -8   -7   -6   -5   -4  -3 -2 -1
print(lista)  # Imprime a lista
print(lista[::-1])  # Inverte a lista
print(lista[3:5])  # Imprime ['D', 'E', 1, 2, 3]
print(lista[:3])  # Imprime ['A', 'B', 'C']
print(lista[::2])  # Step 2: ['A', 'C', 'E', 2]
