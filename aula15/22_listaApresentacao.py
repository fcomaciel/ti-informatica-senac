"""
Listas em Python
- fatiamento
- append, insert, pop, del, clear, extend, +
- min, max
- range

O conjunto completo dos métodos do tipo de dado lista pode ser visto em:
https://docs.python.org/pt-br/3/tutorial/datastructures.html#more-on-lists
"""
# Listas também têm índices.
# E se tem índice, é iterável.
#         0    1    2    3    4   5  6  7
lista = ['A', 'B', 'C', 'D', 'E', 1, 2, 3]
#        -8   -7   -6   -5   -4  -3 -2 -1

print(len(lista))

print(lista[7])

print(lista[-8])
