# --------------------------
# IF, IF ELSE, IF ELIF, ELSE
# --------------------------

# if False:  # se
#     print('Passe teste lógico 1')
# elif False:  # então se
#     print('Passe teste lógico 2')
# else:  # senão
#     print('Passe teste lógico 3')

num1 = int(input('Digite um número: '))
num2 = int(input('Digite outro número: '))

if num1 > num2:
    print(f'{num1} é maior do que {num2}')  # OP1
elif num1 < num2:
    print(f'{num1} é menor do que {num2}')  # OP2
else:
    print(f'{num1} é igual a {num2}')  # OP3
