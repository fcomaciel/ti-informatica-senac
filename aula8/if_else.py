# --------------------------
# IF, IF ELSE, IF ELIF ELSE
# --------------------------

# if False:  # se
#     print('Verdadeiro')
# else:  # senão
#     print('não é verdadeiro')
# print('Esse texto será sempre impresso')

num = 2

if num <= 3:
    print('num é 3')
    print(num - 3)
else:
    print('num não é 3')
    print(num - 3)
print('Programa encerrado com sucesso')
