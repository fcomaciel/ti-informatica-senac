totalSegundos = int(input('Informe o total de segundos: '))

horas = totalSegundos / 3600
minutos = totalSegundos % 3600 / 60
segundos = totalSegundos % 60  # Aqui um comentário

print('Resultado: {0}:{1}:{2}'.format(int(horas), int(minutos), segundos))
