'''
Escreva um programa que simula o valor da prestação de
uma compra parcelada sem juros de 1 x até 20x. O valor da
compra pode ser digitado pelo usuário (use o comando "input").
O valor da prestação sem juros deve ser calculado como o valor
da compra dividido pelo número de prestações, de 1 até 20. Para
verificar se seu programa está correto, digite o valor 1000
e verifique se o programa produz o seguinte resultado:

1x de R$ 1000.00
2x de R$ 500.00
3x de R$ 333.33
4x de R$ 250.00
5x de R$ 200.00
6x de R$ 166.67
7x de R$ 142.86
8x de R$ 125.00
9x de R$ 111.11
10x de R$ 100.00
11x de R$ 90.91
12x de R$ 83.33
13x de R$ 76.92
14x de R$ 71.43
15x de R$ 66.67
16x de R$ 62.50
17x de R$ 58.82
18x de R$ 55.56
19x de R$ 52.63
20x de R$ 50.00
'''

# Modificar o código abaixo para cobrar
# 2% de juros após a 5ª prestação.

valor_compra = input('Informe o valor da compra: ')

for parcela in range(1, 21):
    valor_compra = float(valor_compra)
    if parcela <= 5:
        print(f'{parcela} x de R$ {valor_compra / parcela:.2f}')
    else:
        print(f'{parcela} x de R$ {valor_compra * 1.02 / parcela:.2f}')
