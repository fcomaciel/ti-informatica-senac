"""
Split, Join e Enumerate em Python
1. Split - usado para dividir uma string (str)
2. Join - Usado para juntar uma lista (str)
3. Enumerate - Usado para enumerar elementos de objetos iteráveis
"""

string_a = 'Python é fenomenal, python é para propósito geral'

# lista_a = string_a.split(' ')
# print(lista_a)

lista_b = string_a.split(',')
print(lista_b)

for valor in lista_b:
    print(valor)
    # print(valor.strip())
    # print(valor.strip().capitalize())
