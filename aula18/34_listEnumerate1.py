"""
Split, Join e Enumerate em Python
1. Split - usado para dividir uma string (str)
2. Join - Usado para juntar uma lista (str)
3. Enumerate - Usado para enumerar elementos de objetos iteráveis
"""

lista = [[1, 2], [3, 4], [5, 6]]

for v in lista:
    print(v)

# for v in lista:
#     print(v[0], v[1])

lista1 = [
    [1, 'Juliana'],
    [3, 'Sarah'],
    [5, 'Fátima'],
]

# Desempacotando lista1

for index, name in lista1:
    print(index, name)

# Enumerate - Desempacotando lista2

lista2 = ['Juliana', 'Sarah', 'Fátima']

for index, name in enumerate(lista2):
    print(index, name)
