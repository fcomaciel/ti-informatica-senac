"""
Split, Join e Enumerate em Python
1. Split - usado para dividir uma string (str)
2. Join - Usado para juntar uma lista (str)
3. Enumerate - Usado para enumerar
   elementos de objetos iteráveis
"""

string_a = 'Python é fenomenal, python é para propósito geral.'

lista_b = string_a.split(' ')
string_b = ' '.join(lista_b)

print(string_a)  # Imprime a string_a 
print(lista_b)   # Imprime a string_a convertida em lista_b
print(string_b)  # Imprime a lista_b convertida em string_b 
