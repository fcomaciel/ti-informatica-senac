"""
Módulo random
Este módulo implementa geradores de números
pseudo-aleatórios para várias distribuições.

https://docs.python.org/pt-br/3/library/random.html
"""
import random

""" random.randrange """
# print(random.randrange(1, 10))

""" random.randint """
# x = random.randint(1, 20)
# print(x)

pessoas = ['Maria', 'Carla', 'Teresa', 'Inês', 'Tenório', 'Garcia']

""" random.choice """
# sorteio = random.choice(pessoas)
# print(sorteio)

""" random.shuffle """
# random.shuffle(pessoas)
# print(pessoas)

""" random.sample """
# print(random.sample(pessoas, k=3))

# result = random.sample(range(0, 100), 4)
# print(result)

""" Retorna número de ponto flutuante aleatório no intervalo [0.0, 1.0). """
# print(random.random())
