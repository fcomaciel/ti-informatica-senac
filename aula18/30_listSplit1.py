"""
Split, Join e Enumerate em Python
1. Split - usado para dividir uma string (str)
2. Join - Usado para juntar uma lista (str)
3. Enumerate - Usado para enumerar elementos de objetos iteráveis
"""

string_a = 'Python é fenomenal, Python é para propósito geral'

lista_a = string_a.split(' ')
print(lista_a)

# Descobrindo qual a palavra campeã em aparição na lista_a
palavra = ''
contagem = 0
for valor in lista_a:
    qtd_vezes = lista_a.count(valor)
    if qtd_vezes > contagem:
        contagem = qtd_vezes
        palavra = valor

print(f'Palavra campeã da lista_a: {palavra} x {contagem}')
