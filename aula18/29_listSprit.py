"""
Split, Join e Enumerate em Python
1. Split - usado para dividir uma string (str)
2. Join - Usado para juntar uma lista (str)
3. Enumerate - Usado para enumerar elementos de objetos iteráveis
"""

string_a = 'Python é fenomenal, Python é para propósito geral'

lista_a = string_a.split(' ')
print(lista_a)

# lista_b = string_a.split(', ')
# print(lista_b)

# Contando quantas vezes cada palavra apareceu na lista_a
for valor in lista_a:
    print(f'A palavra {valor} apareceu', end='')
    print(f'{lista_a.count(valor)} x na frase ')
