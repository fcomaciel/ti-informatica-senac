"""
Split, Join e Enumerate em Python
1. Split - usado para dividir uma string (str)
2. Join - Usado para juntar uma lista (str)
3. Enumerate - Usado para enumerar elementos de objetos iteráveis
"""

string = 'Python é fenomenal, python é para propósito geral.'

lista = string.split(' ')

for indice, valor in enumerate(lista):
    # print(indice, valor)
    # print(indice, valor, lista[indice])
    # print(indice, lista[indice])
    print(lista[indice])
