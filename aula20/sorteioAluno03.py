from random import choice
from time import sleep

lista = []

j1 = ['Adinael Soares Moreira Leal' , 'André Richardson Pierote Almeida' , 'Edigar da Luz Júnior' , 'Ângelo Matheus Campelo Da Silva' , 'Antonio Marcos Magalhaes Chaves' , 'Carlos Daniel da Silva Barbosa' , 'Denis Kelvis Mends Bezerra' , 'Rafael Nery Coelho']
j2 = ['Francisco Fernando Rodrigues Lopes Feitosa' , 'Gabriel Guimarães De Almeida' , 'Gabriel silva almeida' , 'Guilerme Amorim Ferreira' , 'Henrique Vieira da Silva' , 'Iago Cardoso do Nascimento' , 'José Eduardo de Sousa Braga' , 'Rafael Araujo de Abreu' , 'Victor Emanoel de Brito Sousa' ]
j3 = ['Lucas Vitor Vieira do Nascimento' , 'Luciano Angelo de Oliveira' , 'Luiz Matheus Soares Batista da Cunha' , 'Othon Gilson Carvalho de Oliveira' , 'Patrick Ariel Oliva Rodrigues' , 'Paulo Fernando Leal da Silva' , 'Pedro Azevedo Rego' , 'Ramon Ryquelme Rodrigues Oliveira' , 'Romenson Cleiton de Sousa Silva' ,]

for c in range(1, 11):
    print('PROCESSANDO....')
    sleep(2)
    print('-' * 20)
    print(f'     \033[34mEQUIPE {c}\033[m')
    print('-' * 20)
    sorteio = (f'{choice(j1)}\n'
               f'{choice(j2)}\n'
               f'{choice(j3)}')
    print(sorteio)
    print('-' * 20)