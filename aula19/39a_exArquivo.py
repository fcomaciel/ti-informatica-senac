"""
Depois que um arquivo é aberto e você tem um objeto de arquivo,
você pode obter várias informações relacionadas a esse arquivo.
"""
# Verificanco o estado do arquivo
fo = open("exemplo.txt", "wb")
print("Nome do arquivo: ", fo.name)
print("Fechado ou não : ", fo.closed)
print("Modo aberto : ", fo.mode)
fo.close()
