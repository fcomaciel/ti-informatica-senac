"""
O módulo "os" do Python fornece métodos que ajudam
você a realizar operações de processamento de arquivos,
como renomear e excluir arquivos.

Para usar este módulo, você precisa importá-lo primeiro
e depois pode chamar qualquer função relacionada.
"""
import os

"""
Syntax:
os.rename(current_file_name, new_file_name)
"""

# Renomear o arquivo de exemplo.txt para exemplo1.txt
os.rename('exemplo.txt', 'exemplo1.txt')
