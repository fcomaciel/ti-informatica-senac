# Abre o arquivo em modo r+ (leitura e escrita.)
fo = open("exemplo.txt", "r+")
str = fo.read(10)
print("A sequência de leitura é : ", str)

# Checa a posição do ponteiro
position = fo.tell()
print("Posição atual do arquivo : ", position)

# Nova leitura antes de reposicionar o ponteiro
str = fo.read(10)
print("Nova leitura : ", str)

# Reposiciona o ponteiro no início mais uma vez
position = fo.seek(0, 0)
str = fo.read(10)
print("Lendo novamente a String : ", str)

# Fechar arquivo aberto
fo.close()
