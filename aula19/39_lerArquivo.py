"""
w - Abre um arquivo somente para gravação. Substitui
o arquivo se o arquivo existir. Se o arquivo não
existir, cria um novo arquivo para gravação.
"""
# Exemplo
fo = open('exemplo.txt', 'r')
for linha in fo:
    print(linha.strip())
fo.close()
