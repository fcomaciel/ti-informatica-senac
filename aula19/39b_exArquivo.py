"""
w - Abre um arquivo somente para gravação. Substitui
o arquivo se o arquivo existir. Se o arquivo não
existir, cria um novo arquivo para gravação.
"""
# Abre o arquivo
fo = open('exemplo.txt', 'w')
# Escreve 2 linhas
fo.write('Python é uma ótima linguagem?\nSim, é ótima!!\n')
# fecha o arquivo
fo.close()
