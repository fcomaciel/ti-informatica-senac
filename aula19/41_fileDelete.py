"""
O módulo "os" do Python fornece métodos que ajudam
você a realizar operações de processamento de arquivos,
como renomear e excluir arquivos.

Para usar este módulo, você precisa importá-lo primeiro
e depois pode chamar qualquer função relacionada.
"""
import os

"""
Syntax:
os.remove(file_name)
"""

# Renomear o arquivo de exemplo.txt para exemplo1.txt
os.remove('exemplo1.txt')
