"""
Iterando strings com while em Python

Criando uma nove string à partir da iteração
de uma strings já existente. Ajustando todos
solicitando do usuário  qual  letra ele quer
tornar Maiúscula.
"""

# Índices
#        0123456789......................33
frase = 'o rato roeu a roupa do rei de roma'
tamanho_frase = len(frase)
contador = 0
nova_string = ''

caractere = input('Qual caractere deseja colocar MAIÚSCULO? ')

while contador < tamanho_frase:
    letra = frase[contador]
    if letra == caractere:
        nova_string += caractere.upper()
    else:
        nova_string += letra
    contador += 1

print(nova_string)
