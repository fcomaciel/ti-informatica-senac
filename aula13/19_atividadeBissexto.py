"""
Modificar o "18_atividadeBissexto" para imprimir todos os anos de
2004 a 2096 porém indicando anos normais e anos bissextos.

1. Se o ano for uniformemente divisível por 4, vá para a etapa 2. Caso
   contrário, vá para a etapa 5.
2. Se o ano for uniformemente divisível por 100, vá para a etapa 3. Caso
   contrário, vá para a etapa 4.
3. Se o ano for uniformemente divisível por 400, vá para a etapa 4. Caso
   contrário, vá para a etapa 5.
4. O ano é bissexto (tem 366 dias).
5. O ano não é um ano bissexto (tem 365 dias).

Amostra da saída com corte:

2004 - Bissexto (tem 366 dias).
2005 - Não bissexto (tem 365 dias).
2006 - Não bissexto (tem 365 dias).
2007 - Não bissexto (tem 365 dias).
...
...
2093 - Não bissexto (tem 365 dias).
2094 - Não bissexto (tem 365 dias).
2095 - Não bissexto (tem 365 dias).
2096 - Bissexto (tem 366 dias).
"""
