"""
Estrutura de repetição

Recapitulando for in em Python
Iterando strings com for
Função range(start=0, stop, step=1)
"""
# Os múltiplos de 8 de 0 a 50
for n in range(0, 50, 8):
    print(n)

# Equivale a:
print('-----')  # Separador dos resultados
for n in range(50):
    if n % 8 == 0:
        print(n)
