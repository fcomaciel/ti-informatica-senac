'''
Escreva um programa que imprima todos os anos bissextos do
século XXI. Lembre-se que o primeiro ano bissexto do século
foi 2004 e que o último será 2096: e que anos bissextos
ocorrem usualmente de 4 em 4 anos. Assim, a lista que o programa
vai imprimir deve ser 2004, 2008, 2012, 2016, ..., 2092, 2096.
'''
