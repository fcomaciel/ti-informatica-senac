"""
Iterando string com while em Python

Criando uma nova string à partir da iteração
de uma string já existente.
"""

# Índices
#        0123456789......................33
frase = 'o rato roeu a roupa do rei de roma'
tamanho_frase = len(frase)
contador = 0
nova_string = ''

while contador < tamanho_frase:
    letra = frase[contador]
    nova_string += letra
    contador += 1

print(nova_string)
# print(nova_string.title())

# for i in nova_string:
#     print(i)
