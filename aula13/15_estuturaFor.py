"""
Estrutura de repetição

For in em Python
Iterando strings com for
Função range(start=0, stop, step=1)
"""

texto = 'Python'

c = 0
while c < len(texto):
    print(texto[c])
    c += 1

# for c in texto:
#     print(c)

# for n, c in enumerate(texto):
#     print(n, c)
