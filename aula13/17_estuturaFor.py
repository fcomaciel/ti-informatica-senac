"""
Estrutura de repetição

Recapitulando for in em Python
Iterando strings com for
Função range(start=0, stop, step=1)
"""
texto = 'Python'
nova_string = ''

# Substituindo caracteres da String com for
for letra in texto:
    if letra == 't':
        nova_string = nova_string + letra.upper()
    elif letra == 'h':
        nova_string += letra.upper()
    else:
        nova_string += letra

print(nova_string)
