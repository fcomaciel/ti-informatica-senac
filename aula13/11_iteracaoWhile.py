"""
Iterando strings com while em Python
"""

# Índices
#        0123456789.........................35
frase = 'Centro de Educação Profissional (SRB)'  # Iterável
tamanho_frase = len(frase)
contador = 0

# Fazendo aqui a iteração
while contador < 10:
    print(contador)
    contador += 1

# while contador < len(frase):
#     print(frase[contador], contador)
#     contador += 1
