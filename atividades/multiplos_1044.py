"""
BEE 1044
Leia 2 valores inteiros (A e B). Após, o programa deve mostrar
uma mensagem "Sao Múltiplos" ou "Nao sao Múltiplos", indicando
se os valores lidos são múltiplos entre si.

Entrada
A entrada contém valores inteiros.

Saída
A saída deve conter uma das mensagens conforme descrito acima.

Valores para teste:

Entrada     Saída
6 24        São Múltiplos
6 25        Não sao Múltiplos
"""
