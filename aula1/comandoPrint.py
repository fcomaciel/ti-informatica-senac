# print('Senac', 'Piauí', sep=' - ', end='')
for x in range(1, 11):
    print(x)

for y in ('Python'):
    print(y)

# Exercício 1:
# Escreva um programa com base nos recursos
# mostrados acima, mas que escreva no terminal
# as informações no seguinte layout:
'''
1 2 3 4 5 6 7 8 9 10
Python
'''
for x in range(1, 11):
    print(x, end=' ')
print()
for y in ('Python'):
    print(y, end='')
print()

# Exercício 2:
# Utilize a função PRINT o separador SEP=
# e o finalizador END= para escrever o seguinte
# CPF na tela do terminal:
'''
CPF: 824.176.070-18
'''

print('CPF: 824', '176', '070', sep='.', end='-')
print('18')

print('texto1', end=' ')
print('texto2', end= ' ')
print()
print('texto3')
