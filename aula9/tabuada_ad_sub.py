"""
Atividade 7 - aula09
Faça um algoritmo para calcular a tabuada de adição,
ou subtração, de um número que o usuário desejar. Neste
caso o usuário entrará com o número e a operação, exemplo:

Exemplo entrada:
- numero: 5
- operacao: +

Saída:
Tabuada de soma do 5
1 + 5 = 6
2 + 5 = 7
3 + 5 = 8
4 + 5 = 9
5 + 5 = 10
6 + 5 = 11
7 + 5 = 12
8 + 5 = 13
9 + 5 = 14
10 + 5 = 15
"""

num = int(input('Digite um número: '))
operacao = input('Qual a operação (+, -): ')

contador = 1
for i in range(1, 11):
    if operacao == '+':
        print(f'{contador} + {num} = {num + contador}')
        contador += 1
    else:
        print(f'{num} - {contador} = {num - contador}')
        contador += 1
