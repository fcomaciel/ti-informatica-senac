"""
Atividade 8 - aula09
Faça um algoritmo que imprima toda
atabuadade multipicação de 1 a 10:
"""
for numero in range(1, 11):
    print(f'Tabuada de {numero}')
    print('-------------')
    for tabuada in range(1, 11):
        print(f'{tabuada} x {numero} = {tabuada * numero} ')
    print('\n')
