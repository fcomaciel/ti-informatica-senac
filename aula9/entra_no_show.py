"""
Atividade 6 - aula09
Faça um algoritmo que pergunta o nome e a idade da pessoa
que está querendo entrar no show do Zé vaqueiro.

Se a pessoa for maior de idade, imprima que ela pode entrar,
senão, ela não poderá entrar!

Detalhe: seu código deve perguntar para todas as pessoas que
estão na fila. Quando o usuário digitar 0 o código será encerrado.
"""

while True:
    print('Para encerrar digite s')
    nome = input('Qual o seu nome: ')
    if nome.lower() == 's':
        break
    else:
        idade = int(input('Informe a sua idade: '))
        if idade <= 18:
            print(f'Entrada não permitida para: {nome}')
        else:
            print(f'Entrada liberada para: {nome}')
print('Programa encerrado com sucesso!')
