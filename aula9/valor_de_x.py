'''
Atividade 4 - aula09
Faça um código que receba um número inteiro.
E imprima os seguintes valores:
Seu número é: X
O antecessor deste número é: X - 1
O sucesso deste número é: X + 1
X * 10 =
X ** 2 =
X / 2 =
X % 2 =
'''

x = int(input('Digite um valor para "X": '))

print(f'Seu número é: {x}')
print(f'O antecessor deste número é: {x - 1}')
print(f'O sucesso deste número é: {x + 1}')
print(f'{x} * 10: {x * 10}')
print(f'{x} ** 2: {x ** 2}')
print(f'{x} / 2: {x / 2}')
print(f'{x} % 2: {x % 2}')
