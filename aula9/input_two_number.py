'''
Atividade 2 - aula09
Faça um código que solicite do usuário 2 números
inteiros (2 input) e imprima na tela do terminal,
usando apenas 1 print(), as operações de adição e
subtração dos números digitados. Use a função
.format() na impressão.
'''

num1 = int(input('Digite um número inteiro: '))
num2 = int(input('Digite outro número inteiro: '))

adicao = num1 + num2
subtracao = num1 - num2

print('{0} + {1} = {2} | {0} - {1} = {3}'.format(
    num1, num2, num1 + num2, num1 - num2))
