'''
Atividade 1 - aula09
Faça um código que leia, (função input()), um número
inteiro digitado pelo usuário e imprima, (função print()),
na tela do terminal se o número é par ou ímpar.

OBS.: Um número é par se o resto da divisão (%) por 2 for igual a zero.
'''

numero = int(input('Digite um número inteiro: '))

if numero % 2 == 0:
    print(f'{numero} é um número PAR!')
else:
    print(f'{numero} é um número ÍMPAR!')
