# VARIÁVEIS
'''
CONVENÇÃO DE NOMECLATURA
Camel Case:
- nomeCompleto
- salarioDoProgramador
- idadeAtual
- salarioAtual
Capital case ou Pascal case
- NomeCompleto
- SalarioDoProgramador
- IdadeAtual
- SalarioAtual
Snake case
- nome_completo
- salario_do_programador
- idade_atual
- salario_atual
'''

nome = 'Raimundo Nonato'  # Aqui um comentário
idade = 38
salário = 2222.22

print('\nNome:', nome, '\nIdade:', idade, '\nSalário: R$', salário)
