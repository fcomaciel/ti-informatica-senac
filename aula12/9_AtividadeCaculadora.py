"""
While em Python
utilizado para realizar ações enquanto
uma condição for verdadeira.

Atividade:
----------
Desenvolva uma calculadora com a capacidade de efetuar
as opções de operações +, -, *, /, **, // e %. Abaixo
você já encontra o esqueleto do programa e as orientações
para cada um dos blocos de código a ser programado.
"""
while True:
    num_1 = input('Digite um número: ')
    num_2 = input('Digite outro número: ')
    operador = input('Digite um operador ( +, -, *, /, **, // ou % ): ')

    if not num_1.isnumeric() or not num_2.isnumeric():
        print('Error')
        continue

    num_1 = int(num_1)
    num_2 = int(num_2)

    if operador == '+':
        somar = num_1 + num_2
        print(f'A soma de {num_1} + {num_2} = {somar}')

    elif operador == '-':
        subtrair = num_1 - num_2
        print(f'A subtrair de {num_1} - {num_2} = {subtrair}')

    elif operador == '*':
        multiplicar = num_1 * num_2
        print(f'A multiplicação de {num_1} * {num_2} = {multiplicar}')

    elif operador == '/':
        dividir = num_1 / num_2
        print(f'A divisão de {num_1} / {num_2} = {dividir}')

    elif operador == '**':
        potencia = num_1 ** num_2
        print(f'A pntenciação de {num_1} ** {num_2} = {potencia}')

    elif operador == '//':
        divideInt = num_1 // num_2
        print(f'Divisão inteira {num_1} // {num_2} = {divideInt}')

    elif operador == '%':
        resto = num_1 % num_2
        print(f'Resto de {num_1} % {num_2} = {resto}')

    else:
        print('Digite um operador válido!')
        continue

    sair = input('Deseja encerrar: [S]im ou [N]ão ')

    if sair.lower() == 's':
        break
