"""
While em Python
utilizado para realizar ações enquanto
uma condição for verdadeira.

Requisitos: Entender condições e operadores.
"""

x = 0

while x < 5:
    if x == 3:
        x += 1
        # break
        continue

    print(f'Laço atual: {x}')
    x += 1
print('Laço encerrado!')
