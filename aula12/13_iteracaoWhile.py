"""
Iterando strings com while em Python

Criando uma nove string à partir da iteração
de uma strings já existente. Ajustando todos
os caracteres r de minúsculo para maiúsculo.
"""

# Índices
#        0123456789......................33
frase = 'o rato roeu a roupa do rei de roma'
tamanho_frase = len(frase)
contador = 0
nova_string = ''

caractere = input('Informe o caracter para maúscula: ')

while contador < tamanho_frase:
    letra = frase[contador]
    if letra == caractere:
        nova_string += caractere.upper()
    else:
        nova_string += letra
    contador += 1

print(nova_string)
