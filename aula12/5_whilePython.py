"""
While em Python
utilizado para realizar ações enquanto
uma condição for verdadeira.

Requisitos: Entender condições e operadores.
"""

while True:
    nome = input('Qual o seu nome?: ')
    print(f'Olá {nome}')

    sair = input('Deseja Sair? ')
    # if sair == 's' or sair == 'S':
    if sair.lower() == 's':
        break
