"""
While em Python
utilizado para realizar ações enquanto
uma condição for verdadeira.

Requisitos: Entender condições e operadores.
"""

x = 0

while x < 5:
    print(f'Laço atual: {x}')
    x += 1  # É necessário incrementar o valor de x para não se tornar infinito
    # x = x + 1
print('Laço encerrado!')
