"""
While em Python
utilizado para realizar ações enquanto
uma condição for verdadeira.

Requisitos: Entender condições e operadores.
"""

x = 0

while x < 10:
    y = 0
    while y < 5:
        # print(f'({x}, {y})')  # Gerando uma matriz
        y += 1
        z = 0
        while z < 5:
            print(f'({x}, {y}, {z})')
            z += 1
    x += 1

print('Encerrou!')
