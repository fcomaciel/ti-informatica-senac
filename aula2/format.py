txt = 'For only {price:.2f} dollars!'
print(txt.format(price = 49))

txt1 = "My name is {fname}, I'm {age}".format(fname="Jhon", age=36)
print(txt1)

txt3 = 'The temperature is between {:-} and {:-} degrees celsius.'
print(txt3.format(-3, 7))
