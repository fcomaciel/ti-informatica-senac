# VARIÁVEIS - MÉTODO STR.FORMAT

nome = 'Raimundo Nonato'
idade = 38
salario = 2222.22

# Por ordem natural
print('Nome: {} Idade: {} Salário: R$ {}'.format(nome, idade, salario))

# Por ordem natural por indicação
print('Nome: {0} Idade: {1} Salário: R$ {2}'.format(nome, idade, salario))

# Pelo indentificador da variável
print('Teste1: {t1} Teste2: {t2}'.format(t1 = 'Teste1', t2 = 'Teste2'))
