"""
Operadores aritméticos

Operador    Ação
+           Adição              2 + 5
-           Subtração           2 - 5
*           Multiplicação       2 * 5
/           Divisão             2 / 5
//          Divisão inteiro     22 // 5
**          Potenciação         2 ** 8
%           Resto de divisão    2 % 5
()          Procedência         (5 + 2) * 10 = 70
"""

print('Adição: 2 + 5 = {}'.format(2 + 5))
print('Subtração: 2 - 5 = {}'.format(2 - 5))
print('Multiplicação: 2 * 5 = {}'.format(2 * 5))
print('Divisão: 2 / 5 = {}'.format(2 / 5))
print('Divisão inteiro: 22 // 5 = {}'.format(22 // 5))
print('Potenciação: 2 ** 8 = {}'.format(2 ** 8))
print('Resto: 2 % 5 = {}'.format(2 % 5))
print('Sem procedência: 5 + 2 * 10 = {}'.format(5 + 2 * 10))
print('Com procedência: (5 + 2) * 10 = {}'.format((5 + 2) * 10))
