# MÉTODO F-STRING

escola = 'Senac - Sistema Nacional de Aprendizagem Comercial - AR / PI'
cep = 'Severino Ramos Brasil'
curso = 'Técnico em Informática'
laboratorio = '022'
aluno = 'Celina Vieira Braga'

print(f'Escola: {escola.upper()}')
print(f'CEP: {cep}')
print(f'Curso: {curso}')
print(f'Laboratório: {laboratorio}')
print(f'Aluna (o): {aluno}')
print()

# CTRL + ALT + SETA PARA BAIXO
