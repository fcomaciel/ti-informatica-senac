# VARIÁVEIS - MÉTODO NUM.FORMAT

# Formatação por tipo

num1 = 12  # int
num2 = 5   # int
num3 = 1.8456789123 # float
divisao = 1 / 3

print('Num1: {:e} \nNum2: {:d} \nNum3: {:.2f} \nDivisão {}'.format(num1, num2, num3, divisao))
