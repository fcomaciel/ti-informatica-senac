"""
Atividade 1 - aula10
Escreva um programa que solicite do usuário uma entrada em hora, tipo 12:45 e
se dependendo hora digitada escreva uma saudação "Bom dia!" de 0 às 11,
"Boa tarde!" de 12 às 17, senão escreva "Boa noite".

Valide a entrada para que o usuário não BUGIE o programa digitando valoes < 0
e > 24. Caso ele o faça mostre a mensagem
O horário digitado está em incorreto.

Valores para teste:

Valor digitado  Resultado
11:59           Bom dia!
12:00           Bom tarde!
17:59           Bom tarde!
18:00           Boa noite!
23:59           Boa noite!
00:00           Bom dia!
24:00           O horário digitado 24:0 está em formato incorreto.
-1:14           O horário digitado -1:14 está em formato incorreto.
"""
hora = input('Digite a hora no formato (hh:mm): ')

minuto = int(hora.split(':')[1])
hora = int(hora.split(':')[0])

# Verificando se o termo digitado corresponde ao horário 0 a 24 horas

if (int(hora) < 0 or int(hora) > 23) or (int(minuto) < 0 and int(minuto) > 59):
    print(f'O horário digitado {hora}:{minuto} está incorreto!')
else:
    if hora >= 0 and hora <= 11:
        print('Bom dia !')
    elif hora >= 12 and hora <= 17:
        print('Boa tarde !')
    else:
        print('Boa noite !')
