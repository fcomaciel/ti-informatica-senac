'''
Atividade 2 - aula10

Escreva um programa que solicite ao usuário seu primeiro nome.
Se o nome contiver 4 letras ou menos escreva "Seu nome é muito
curto"; se tiver entre 5 e 6 letras escreva "Seu nome é normal;
se maior do 6 escreva "Seu nome é muito grande".

Valores para teste:

Valor digitado  Resultado
Zé				Seu nome é muito curto
Maria			Seu nome é normal
Juliana			Seu nome é muito grande
'''
nome = input('Digite o seu nome: ')

comprimento = len(nome)

if comprimento <= 4:
    print('Seu mome é muito curto.')
elif comprimento >= 5 and comprimento <= 6:
    print('Seu nome é normal.')
else:
    print('Seu nome é muito grande.')
