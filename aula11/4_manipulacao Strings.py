"""
Manipulação de Strings
* Strings indices
* Fatiamento de strings [inicio:fim:passo]
* Funções built-in len, abs, type, print, etc...

Essas funções podem ser usadas diretamente em cada tipo.

Vocês podem conferir tudo isso em:

https://docs.python.org/3/library/stdtypes.html (tipos built-in)
https://docs.python.org/3/library/functions.html (funções built-in)
"""
#       [012345678]  <- Índices  positivos
texto = 'Python V3'

print( len(texto) )  # A função len() informa quantos caracteres há na String

# Imprimindo o caractere da 3ª posição.
print(texto[2])

# Imprimindo o caractere da 9ª  posição.
print(texto[8])

#      -[876543210]  <- Índices  negativo
texto = 'Python V3'

# Imprimindo os índices de 0 a 6 (ou seja, 9 - 3 = 6)
print(texto[:-3])

# Imprimindo os índices de 0 e 1 (ou seja, 9 - 7 = 2)
print(texto[:-7])

# Exemplo usual

url = 'www.pi.senac.br/'

print(url)

# Vamos retirar  a "/" da URL
print(url[:-1])

#### FATIAMENTO  ###

texto = 'Python V3'

nova_string = texto[2:6]  # Recorta do índice 2 ao 5 (o 6 não é incluso!).
print(nova_string)

nova_string = texto[:6]  # Recorta do índice 0 ao 5 (o 6 não é incluso!).
print(nova_string)

nova_string = texto[7:]  # Recorta do índice 8 ao 9.
print(nova_string)

#### FATIAMENTO ÍNDICE NEGATIVO ###

texto = 'Python V3'

nova_string = texto[-9:-3]  # Recorta do índice -9 ao -2 (o -3 não é incluso).
print(nova_string)

nova_string = texto[:-1]  # Retira o último caractere da string.
print(nova_string)

#### FATIAMENTO UTILIZANDO PASSO: [inicio:fim:passo] ###

##      [012345678]  <- Índices  positivos
texto = 'Python V3'

nova_string = texto[0:6:2]  # Fatia do índice 0 ao 5 com passo 2 em 2
print(nova_string)

texto = 'Python V3'

nova_string = texto[0::3]  # Faz o fatiamento da string total com passo 3 em 3
print(nova_string)

# Esse recurso permite explorar novas práticas com Python

texto = 'Python V3'

for letra in texto:
    print(letra, end='')
