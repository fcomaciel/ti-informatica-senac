'''
Formatando valores com modificadores

:s  - Texto (String)
:d	- Inteiros (int)
:f	- Número de ponto flutuante (float)
:.(NÚMERO)float	- Quantidade de casas decimais (float)
:(CARACTERE)(>  ou < ou ^)  (QUANTIDADE) (TIPO - s, d  ou f)

>	- Esquerda
<	- Direita
^	- Centro
'''

curso = 'Técnico de  Informática'

print(curso.lower())
print(curso.upper())
print(curso.title())
print(curso.ljust(30, '#'))
print(curso.rjust(30, '#'))
