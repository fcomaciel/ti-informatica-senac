'''
Formatando valores com modificadores

:s  - Texto (String)
:d	- Inteiros (int)
:f	- Número de ponto flutuante (float)
:.(NÚMERO)float	- Quantidade de casas decimais (float)
:(CARACTERE)(>  ou < ou ^)  (QUANTIDADE) (TIPO - s, d  ou f)

>	- Esquerda
<	- Direita
^	- Centro
'''

num1 = 10
num2 = 3
nome = 'Senac'

divisao = num1 / num2

print(f'Divisão sem formatação: {divisao}')
print(f'Divisão com formatação: {divisao:.2f}')


num1 = 1
num2 = 1155

print(f'{num1:0>10}')  # 10 dígitos sendo complementado com zeros à esquerda

print(f'{num2:0>10}')

print(f'{num1:0<10}')  # 10 dígitos sendo complementado com zeros à direita

print(f'{num2:0<10}')

print(f'{num2:0>10.2f}')  # Combinado 10 dígitos e 2 casas decimais

print(f'{num2:0<10.2f}')

print(f'{num2:0^10}')  # Tamanho 10 dígitos centralizado

print(f'{nome:#^10}')  # Também funciona com strings
