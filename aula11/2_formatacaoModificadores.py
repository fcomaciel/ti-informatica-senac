'''
Formatando valores com modificadores

:s  - Texto (String)
:d	- Inteiros (int)
:f	- Número de ponto flutuante (float)
:.(NÚMERO)float	- Quantidade de casas decimais (float)
:(CARACTERE)(>  ou < ou ^)  (QUANTIDADE) (TIPO - s, d  ou f)

>	- Esquerda
<	- Direita
^	- Centro
'''

curso = 'Técnico de  Informática'

print()

formatado = '{}\n'.format(curso)  # Sem formatação
print(formatado)

formatado = '{:#>30}\n'.format(curso)
print(formatado)

formatado = '{:#<30}\n'.format(curso)
print(formatado)

formatado = '{:#^30}\n'.format(curso)
print(formatado)
